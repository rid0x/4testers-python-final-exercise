## Python Programming - Final Project

This repository contains the final project for the Python programming course.

### Project Description

The project consists of two classes implemented in the `shop.py` file:

1. `Product` class: Represents a product in a store, with attributes such as name, unit price, and quantity.
2. `Order` class: Represents an order in a store, allowing the addition of products, calculation of the total price, total quantity of products, and purchase status.

The goal of this project is to complete the methods in both classes based on the provided tests. The task is to modify the code in such a way that the tests pass successfully without changing the test code itself.

### File Structure

The repository is structured as follows:

```
4testers-python-final-exercise/
├── src/
│   └── shop.py
└── tests/
    └── test_shop.py
```

- The `src` directory contains the `shop.py` file, which contains the implementation of the `Product` and `Order` classes.
- The `tests` directory contains the `test_shop.py` file, which contains the test cases for the `Product` and `Order` classes.

